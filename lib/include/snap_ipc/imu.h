/**
 * @file imu.h
 * @brief API for accessing IMU data via Modal Pipe Architecture
 * @author Parker Lusk <plusk@mit.edu>
 * @date 28 June 2021
 */

#pragma once

#include <cstdint>
#include <functional>
#include <string>
#include <vector>

#include <modal_pipe.h>

namespace acl {
namespace snapipc {

  // Select the better of the two IMUs onboard the VOXL. See
  // https://docs.modalai.com/camera-imu-coordinate-frames
  static constexpr const char * DEFAULT_IMU = "imu1";

  class IMU
  {
  public:
    /**
     * @brief      IMU data returned via callback
     */
    struct Data
    {
      uint64_t usec; ///< timestamp in seconds
      uint32_t seq; ///< sequence number
      float acc_x, acc_y, acc_z; ///< units of [g/s^2]
      float gyr_x, gyr_y, gyr_z; ///< units of [rad/s]
    };

    // signature to use when registering a callback
    using Callback = std::function<void(const std::vector<Data>& samples)>;

  public:
    IMU(const std::string& pipe_path = DEFAULT_IMU);
    ~IMU();

    /**
     * @brief      Register a callback that is fired when
     *             batches of IMU data are available.
     *
     * @param[in]  cb    Callback conforming to Callback signature.
     */
    void register_imu_cb(Callback cb);

    /**
     * @brief      Connection status
     */
    bool is_connected() const { return connected_; }

    /**
     * @brief      Returns the current IMU packet number
     *
     * @return     Sequence number
     */
    int get_seq() { return seq_++; }

  private:
    //\brief Pipe constants
    static constexpr int IMU_CH = 0; ///< arbitrary channel number
    static constexpr const char * CLIENT_NAME = "snap-io-imu-client";

    std::string pipe_path_; ///< imu pipe name to connect to (e.g., "imu1")

    bool connected_ = false; ///< indicates if pipe is connected to server
    int seq_ = 0; ///< sequence number used for each imu packet

  // \brief The following are public due to c-style callbacks
  public:
    void set_connected() { connected_ = true; }
    void set_disconnected() {connected_ = false; }
    Callback cb_; ///< user's callback
  };

} // ns snapipc
} // ns acl
