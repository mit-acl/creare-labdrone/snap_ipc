/**
 * @file client.h
 * @brief API for accessing snapio ioboard via mpa
 * @author Parker Lusk <plusk@mit.edu>
 * @date 28 June 2021
 */

#pragma once

#include <atomic>
#include <array>
#include <cstdint>
#include <iostream>
#include <mutex>
#include <stdexcept>
#include <string>
#include <thread>

#include <modal_pipe.h>

namespace acl {
namespace snapipc {

  /**
   * Minimum and maximum allowable ESC commands, in microseconds.
   */
  static constexpr uint16_t PWM_MIN_PULSE_WIDTH = 1000;
  static constexpr uint16_t PWM_MAX_PULSE_WIDTH = 2000;

  class Client
  {
  public:
    Client();
    ~Client();

    /**
     * @brief      Sets the throttle value of each motor
     *
     * @param[in]  throttle   Throttle value in [0, 1] for each motor
     */
    void set_motors(const std::array<float, 6>& throttle);

    /**
     * @brief      Set the gripper open or close.
     *
     * @param[in]  open  True if desired to open the gripper
     */
    void set_gripper(bool open);

  private:
    void create_pipes();
    void close_pipes();

  // \brief The following are public due to c-style callbacks
  public:
    void tx_cb(const uint8_t* data, int bytes);
  };

} // ns snapipc
} // ns acl
