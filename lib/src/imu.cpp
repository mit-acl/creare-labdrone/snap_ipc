/**
 * @file imu.cpp
 * @brief API for accessing IMU data via Modal Pipe Architecture
 * @author Parker Lusk <plusk@mit.edu>
 * @date 28 June 2021
 */

#include "snap_ipc/imu.h"

// ----------------------------------------------------------------------------
// C-style Pipe Callbacks
// ----------------------------------------------------------------------------

// called whenever we connect or reconnect to the server
static void _connect_cb(__attribute__((unused)) int ch, void* context)
{
  acl::snapipc::IMU* imu = reinterpret_cast<acl::snapipc::IMU*>(context);
  imu->set_connected();
}


// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, void* context)
{
  acl::snapipc::IMU* imu = reinterpret_cast<acl::snapipc::IMU*>(context);
  imu->set_disconnected();
}


// called when the simple helper has data for us
static void _helper_cb(__attribute__((unused))int ch, char* data, int bytes, void* context)
{
  acl::snapipc::IMU* imu = reinterpret_cast<acl::snapipc::IMU*>(context);

  // validate that the data makes sense
  int n_packets;
  imu_data_t* data_array = pipe_validate_imu_data_t(data, bytes, &n_packets);
  if(data_array == NULL) return;

  std::vector<acl::snapipc::IMU::Data> samples;
  samples.resize(n_packets);

  for (size_t i=0; i<n_packets; i++) {
    samples[i].usec = data_array[i].timestamp_ns * 1e-3;
    samples[i].seq = imu->get_seq();
    samples[i].acc_x = data_array[i].accl_ms2[0];
    samples[i].acc_y = data_array[i].accl_ms2[1];
    samples[i].acc_z = data_array[i].accl_ms2[2];
    samples[i].gyr_x = data_array[i].gyro_rad[0];
    samples[i].gyr_y = data_array[i].gyro_rad[1];
    samples[i].gyr_z = data_array[i].gyro_rad[2];
  }

  imu->cb_(samples);
}

// ============================================================================

namespace acl {
namespace snapipc {

IMU::IMU(const std::string& pipe_path)
  : pipe_path_(pipe_path)
{
  pipe_client_set_simple_helper_cb(IMU_CH, _helper_cb, this);
  pipe_client_set_connect_cb(IMU_CH, _connect_cb, this);
  pipe_client_set_disconnect_cb(IMU_CH, _disconnect_cb, this);

  int ret = pipe_client_open(IMU_CH, pipe_path.c_str(), CLIENT_NAME,
                  EN_PIPE_CLIENT_SIMPLE_HELPER | EN_PIPE_CLIENT_AUTO_RECONNECT,
                  IMU_RECOMMENDED_READ_BUF_SIZE);

  if (ret < 0) {
    pipe_print_error(ret);
  }
}

// ----------------------------------------------------------------------------

IMU::~IMU()
{
  pipe_client_close(IMU_CH);
}

// ----------------------------------------------------------------------------

void IMU::register_imu_cb(Callback cb)
{
  cb_ = cb;
}


} // ns snapipc
} // ns acl