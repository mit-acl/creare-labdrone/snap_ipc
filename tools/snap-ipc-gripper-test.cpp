/**
 * @file snap-ipc-gripper-test.cpp
 * @brief CLI tool to open / close gripper via snap_ipc
 * @author Parker Lusk <plusk@mit.edu>
 * @date 29 April 2021
 */

#include <chrono>
#include <iostream>
#include <sstream>
#include <thread>

#include <snap_ipc/client.h>

void help(int argc, char const *argv[])
{
  std::cout << argv[0] << " [1|0]" << std::endl;
  std::cout << "    " << "1 for open, 0 for close" << std::endl;
  if (argc == 2) {
    std::cout << std::endl;
    std::cout << "    " << "Got " << argv[1] << std::endl;
  }
  std::cout << std::endl;
}

int main(int argc, char const *argv[])
{
  if (argc != 2) {
    help(argc, argv);
    return -1;
  }

  bool open;

  int tmp;
  std::istringstream ss(argv[1]);

  if (!(ss >> tmp)) {
    help(argc, argv);
    return -2;
  } else {
    open = (tmp == 1);
  }

  acl::snapipc::Client client;
  client.set_gripper(open);

  std::this_thread::sleep_for(std::chrono::milliseconds(1000));

  return 0;
}