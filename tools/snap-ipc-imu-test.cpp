/**
 * @file snap-ipc-imu-test.cpp
 * @brief CLI tool to read IMU via snap IPC and mpa
 * @author Parker Lusk <plusk@mit.edu>
 * @date 15 April 2021
 */

#include <getopt.h>
#include <stdio.h>
#include <unistd.h>

#include <cmath>
#include <future>
#include <iomanip>
#include <iostream>
#include <sstream>

#include <snap_ipc/imu.h>
#include <modal_pipe.h>

/// |brief Global variables
uint64_t last_t_us = 0;
char pipe_path[MODAL_PIPE_MAX_PATH_LEN];

static void _print_usage(void)
{
  printf("\n\
Tool to print imu data to the screen for inspection.\n\
\n\
See also: voxl-inspect-imu, voxl-inspect-vibration and voxl-calibrate-imu\n\
\n\
-h, --help                  print this help message\n\
\n\
example usage:\n\
/# snap-io-imu-test imu1\n\
/# snap-io-imu-test /run/mpa/imu0/\n\
\n");
  return;
}

// ----------------------------------------------------------------------------

static int parse_opts(int argc, char* argv[])
{
  static struct option long_options[] =
  {
    {"help",      no_argument,    0,  'h'},
    {0, 0, 0, 0}
  };

  while(1){
    int option_index = 0;
    int c = getopt_long(argc, argv, "ahn", long_options, &option_index);

    if(c == -1) break; // Detect the end of the options.

    switch(c){
    case 0:
      // for long args without short equivalent that just set a flag
      // nothing left to do so just break.
      if (long_options[option_index].flag != 0) break;
      break;
    case 'h':
      _print_usage();
      exit(0);
    default:
      _print_usage();
      exit(0);
    }
  }

  // scan through the non-flagged arguments for the desired pipe
  for(int i=optind; i<argc; i++){
    if(pipe_path[0]!=0){
      fprintf(stderr, "ERROR: Please specify only one pipe\n");
      _print_usage();
      exit(-1);
    }
    if(pipe_expand_location_string(argv[i], pipe_path)<0){
      fprintf(stderr, "ERROR: Invalid pipe name: %s\n", argv[i]);
      exit(-1);
    }
  }

  // make sure a pipe was given
  if(pipe_path[0] == 0){
    fprintf(stderr, "ERROR: You must specify a pipe name\n");
    _print_usage();
    exit(-1);
  }

  return 0;
}

// ----------------------------------------------------------------------------

static void imu_cb(const std::vector<acl::snapipc::IMU::Data>& samples)
{
  // std::cout << "Got " << samples.size() << " samples" << std::endl;
  for (const auto& s : samples) {
    const double dt = (s.usec - last_t_us) * 1e-6; // us to s
    const double hz = 1. / dt;
    last_t_us = s.usec;

    static constexpr int w = 5;
    std::stringstream ss;
    ss << std::fixed << std::setprecision(2)
       << std::setw(w) << std::setfill(' ')
       << s.acc_x << ", "
       << std::setw(w) << std::setfill(' ')
       << s.acc_y << ", "
       << std::setw(w) << std::setfill(' ')
       << s.acc_z
       << '\t'
       << std::setw(w) << std::setfill(' ')
       << s.gyr_x << ", "
       << std::setw(w) << std::setfill(' ')
       << s.gyr_y << ", "
       << std::setw(w) << std::setfill(' ')
       << s.gyr_z;
    std::cout << s.usec << " us: (" << std::setw(4) << std::round(hz) << " Hz): "
              << ss.str() << std::endl;
  }
}

// ----------------------------------------------------------------------------

int main(int argc, char *argv[])
{
  // check for options
  if(parse_opts(argc, argv)) return -1;

  // set some basic signal handling for safe shutdown.
  // quitting without cleanup up the pipe can result in the pipe staying
  // open and overflowing, so always cleanup properly!!!
  enable_signal_handler();
  main_running = 1;

  // Create an IMU object (which connects to the IMU server via mpa)
  acl::snapipc::IMU imu(pipe_path);
  imu.register_imu_cb(imu_cb);

  // spin forever and let CPU do other things (no busy waiting)
  while(main_running) usleep(500000);
  return 0;
}
