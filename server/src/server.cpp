/**
 * @file server.cpp
 * @brief Pipe server for SnapIO ioboard
 * @author Parker Lusk <plusk@mit.edu>
 * @date 29 June 2021
 */

#include <stdio.h>
#include <getopt.h>
#include <unistd.h> // for usleep()
#include <string.h>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>

#include <snap_ipc/server.h>

int main(int argc, char const *argv[])
{
  // check for options
  // if(parse_opts(argc, argv)) return -1;

  // set some basic signal handling for safe shutdown.
  // quitting without cleanup up the pipe can result in the pipe staying
  // open and overflowing, so always cleanup properly!!!
  enable_signal_handler();
  main_running = 1;

  // Create server object which connects via mpa and SnapIO
  acl::snapipc::Server server;

  // spin forever and let CPU do other things (no busy waiting)
  while(main_running) usleep(500000);
  return 0;
}